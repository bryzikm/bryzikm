<?php wp_footer(); ?>
<footer id="footer">
    <a href="#home"><i class="fa fa-chevron-up" aria-hidden="true"></i></a>
    <div class="container">
        <div class="col-xs-12">
            <p>© <span id="year"></span>|<span id="name">MATEUSZ BRYZIK</span></p>
        </div>
    </div>
</footer>
</body>
</html>