<?php get_header(); ?>
<body id="single__project">
<header id="header">
    <div class="container">
        <div class="row">
            <div id="logo" class="col-md-2 col-xs-6">
                <a href="http://bryzikm.pl"><img src="<?php echo get_template_directory_uri(); ?>/img/logo.png"/></a>
            </div>
            <div id="social" class="col-md-2 col-xs-6">
                <div class="social__bar">
                    <a class="contact__linkedin contact__single" href="<?php the_field('contact_linkedin'); ?>">
                        <i class="fa fa-linkedin" aria-hidden="true"></i>
                    </a>
                    <a class="contact__facebook contact__single" href="<?php the_field('contact_facebook'); ?>">
                        <i class="fa fa-facebook" aria-hidden="true"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</header>
<section id="home" style="background-image: url(<?php echo the_field('herobanner'); ?>);">
    <h1><?php the_title(); ?></h1>
</section>
<section id="project__content">
    <div class="container">
        <div class="project__description col-xs-12">
            <div class="col-xs-12">
                <?php the_content(); ?>
            </div>
        </div>
        <div class="project__image col-xs-12">
            <div class="col-xs-12">
                <img src="<?php the_field('image'); ?>" />
            </div>
        </div>
        <div class="project__columns col-xs-12">
            <div class="row">
                <div class="col-md-7 col-xs-12">
                    <div class="project__technologies column col-xs-12">
                        <div class="header__wrapper">
                            <i class="fa fa-cogs" aria-hidden="true"></i>
                            <h2><?php the_field('technologies_header'); ?></h2>
                        </div>
                        <div class="list__wrapper">
                            <?php the_field('technologies'); ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-5 col-xs-12">
                    <div class="project__team column col-xs-12">
                        <div class="header__wrapper">
                            <i class="fa fa-users" aria-hidden="true"></i>
                            <h2><?php the_field('project_team'); ?></h2>
                        </div>
                        <div class="list__wrapper">
                            <?php the_field('team'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="project__nav col-xs-12">
            <div class="row">
                <div class="col-xs-12">
                    <a href="#">Poprzedni</a>
                    <a href="#">Następny</a>
                </div>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>