<?php
/*
    Template Name: Home
*/
?>
<?php get_header(); ?>
<body>
<header id="header">
    <div class="container">
        <div class="row">
            <div id="logo" class="col-md-2 col-xs-6">
                <a href="#home"><img src="<?php echo get_template_directory_uri(); ?>/img/logo.png"/></a>
            </div>
            <nav id="menu" class="col-md-10 hidden-xs hidden-sm">
                <ul>
                    <li><a href="#home">Strona główna</a></li>
                    <li><a href="#about">O mnie</a></li>
                    <li><a href="#services">Usługi</a></li>
                    <li><a href="#skills">Umiejętności</a></li>
                    <li><a href="#projects">Projekty</a></li>
                    <li><a href="#contact">Kontakt</a></li>
                </ul>
            </nav>
            <div id="mobile__button" class="visible-xs visible-sm col-xs-6">
                <i class="fa fa-bars" aria-hidden="true"></i>
            </div>
        </div>
    </div>
    <nav id="mobile__menu" class="hidden-md hidden-lg" style="display: none;">
        <ul>
            <li><a href="#home">Strona główna</a></li>
            <li><a href="#about">O mnie</a></li>
            <li><a href="#services">Usługi</a></li>
            <li><a href="#skills">Umiejętności</a></li>
            <li><a href="#projects">Projekty</a></li>
            <li><a href="#contact">Kontakt</a></li>
        </ul>
    </nav>
</header>
<section id="home" class="section">
    <div id="hero__slider">
        <div class="first__slide single__slide">
        </div>
        <div class="second__slide single__slide">
        </div>
        <div class="third__slide single__slide">
        </div>
    </div>
    <div id="hero__wrapper">
        <div class="container">
            <div id="slider__header">
                <h1><?php the_field('header'); ?></h1>
                <p><?php the_field('name'); ?></p>
                <div class="button__wrapper hidden-xs">
                    <a href="#about"><i class="fa fa-chevron-down" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="about" class="section">
    <div class="container">
        <div class="about__header section__header">
            <h1><?php the_field('about_header'); ?></h1>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-6">
                <div class="left__column column">
                    <img src="<?php the_field('about_image'); ?>"/>
                    <h3><?php the_field('about_first_header'); ?></h3>
                    <p><?php the_field('about_first_subheader'); ?></p>
                </div>
            </div>
            <div class="col-xs-12 col-md-6">
                <div class="right__column column">
                    <div class="first__text text">
                        <h2><?php the_field('about_second_header'); ?></h2>
                        <p><?php the_field('about_second_subheader'); ?></p>
                    </div>
                    <div class="second__text text">
                        <p><?php the_field('about_dark_subheader'); ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="services" class="section">
    <div class="container">
        <div class="services__header section__header col-xs-12">
            <h1><?php the_field('services_header'); ?></h1>
            <p><?php the_field('services_subheader'); ?></p>
        </div>
        <div class="all__services">
            <div class="row">
                <div class="col-md-4">
                    <div class="service">
                        <div class="service__wrapper">
                            <i class="fa fa-file-code-o" aria-hidden="true"></i>
                        </div>
                        <h3><?php the_field('services_first_header'); ?></h3>
                        <p><?php the_field('services_first_subheader'); ?></p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="service">
                        <div class="service__wrapper">
                            <i class="fa fa-refresh" aria-hidden="true"></i>
                        </div>
                        <h3><?php the_field('services_second_header'); ?></h3>
                        <p><?php the_field('services_second_subheader'); ?></p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="service">
                        <div class="service__wrapper">
                            <i class="fa fa-desktop" aria-hidden="true"></i>
                        </div>
                        <h3><?php the_field('services_third_header'); ?></h3>
                        <p><?php the_field('services_third_subheader'); ?></p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="service">
                        <div class="service__wrapper">
                            <i class="fa fa-bar-chart" aria-hidden="true"></i>
                        </div>
                        <h3><?php the_field('services_fourth_header'); ?></h3>
                        <p><?php the_field('services_fourth_subheader'); ?></p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="service">
                        <div class="service__wrapper">
                            <i class="fa fa-paint-brush" aria-hidden="true"></i>
                        </div>
                        <h3><?php the_field('services_fifth_header'); ?></h3>
                        <p><?php the_field('services_fifth_subheader'); ?></p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="service">
                        <div class="service__wrapper">
                            <i class="fa fa-external-link" aria-hidden="true"></i>
                        </div>
                        <h3><?php the_field('services_sixth_header'); ?></h3>
                        <p><?php the_field('services_sixth_subheader'); ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="skills" class="section">
    <div class="container">
        <div class="skills__header section__header col-xs-12">
            <h1><?php the_field('skills_header'); ?></h1>
            <p><?php the_field('skills_subheader'); ?></p>
        </div>
        <div class="circles col-md-12 col-xs-12">
            <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <canvas id="firstCircle" width="200" height="200"></canvas>
                    <h3>Bardzo dobra znajomość</h3>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <canvas id="secondCircle" width="200" height="200"></canvas>
                    <h3>Bardzo dobra znajomość</h3>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <canvas id="thirdCircle" width="200" height="200"></canvas>
                    <h3>Dobra znajomość</h3>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <canvas id="fourthCircle" width="200" height="200"></canvas>
                    <h3>Dobra znajomość</h3>
                </div>
            </div>
        </div>
        <div class="lines col-md-12 col-xs-12">
            <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <h2>jQuery</h2>
                    <div id="line1" class="line">
                        <div></div>
                    </div>
                    <h3>Dobra znajomość</h3>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <h2>JavaScript</h2>
                    <div id="line2" class="line">
                        <div></div>
                    </div>
                    <h3>Dobra znajomość</h3>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <h2>Hibernate</h2>
                    <div id="line3" class="line">
                        <div></div>
                    </div>
                    <h3>Dobra znajomość</h3>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <h2>JPA</h2>
                    <div id="line4" class="line">
                        <div></div>
                    </div>
                    <h3>Dobra znajomość</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <h2>SCSS</h2>
                    <div id="line5" class="line">
                        <div></div>
                    </div>
                    <h3>Dobra znajomość</h3>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <h2>GIT</h2>
                    <div id="line6" class="line">
                        <div></div>
                    </div>
                    <h3>Dobra znajomość</h3>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <h2>WordPress</h2>
                    <div id="line7" class="line">
                        <div></div>
                    </div>
                    <h3>Dobra znajomość</h3>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <h2>HubSpot</h2>
                    <div id="line8" class="line">
                        <div></div>
                    </div>
                    <h3>Dobra znajomość</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <h2>MySQL</h2>
                    <div id="line9" class="line">
                        <div></div>
                    </div>
                    <h3>Dobra znajomość</h3>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <h2>PostgreSQL</h2>
                    <div id="line10" class="line">
                        <div></div>
                    </div>
                    <h3>Dobra znajomość</h3>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <h2>Angular JS</h2>
                    <div id="line11" class="line">
                        <div></div>
                    </div>
                    <h3>Podstawowa znajomość</h3>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <h2>Photoshop</h2>
                    <div id="line12" class="line">
                        <div></div>
                    </div>
                    <h3>Podstawowa znajomość</h3>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="numbers" class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="firstNumber number">
                    <h4><span>0</span>+</h4>
                    <p><i class="fa fa-clock-o" aria-hidden="true"></i>
                        <?php the_field('number_first_text'); ?></p>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="secondNumber number">
                    <h4><span>0</span>k+</h4>
                    <p><i class="fa fa-list-alt" aria-hidden="true"></i>
                        <?php the_field('number_second_text'); ?></p>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="thirdNumber number">
                    <h4><span>0</span>+</h4>
                    <p><i class="fa fa-globe" aria-hidden="true"></i>
                    <?php the_field('number_third_text'); ?></p>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="fourthNumber number">
                    <h4><span>0</span>%</h4>
                    <p><i class="fa fa-check-square-o" aria-hidden="true"></i>
                        <?php the_field('number_fourth_text'); ?></p>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="projects" class="section">
    <div class="container">
        <div class="row">
            <div class="projects__header section__header col-xs-12">
                <h1><?php the_field('projects_header'); ?></h1>
                <p><?php the_field('projects_subheader'); ?></p>
            </div>
        </div>
        <div class="row">
            <div class="projects__buttons">
                <div class="buttons__wrapper">
                    <a href="#" class="button button__all" data-filter=".all">WSZYSTKO</a><span>/</span>
                    <a href="#" class="button button__front" data-filter=".front">WEB DESIGN</a><span>/</span>
                    <a href="#" class="button button__java" data-filter=".java">JAVA</a>
                </div>
            </div>
        </div>
        <div class="projects__wrapper">
            <?php
                $args = array('posts_per_page' => 100);

                $postsArray = get_posts( $args );

                foreach ($postsArray as $singlePost) : setup_postdata( $singlePost );
            ?>
            <div class="col-md-4 col-sm-6 col-xs-12 single__project all <?php echo the_field('isotope', $singlePost->ID); ?>">
                <?php
                    $isotope = get_field('link', $singlePost->ID);

                    if(strcmp($isotope, "") != 0) {
                ?>
                    <a href="<?php echo the_field('link', $singlePost->ID); ?>">
                <?php } else { ?>
                    <a href="<?php the_permalink( $singlePost->ID ); ?>">
                <?php } ?>
                    <div class="project__image"
                        style="background-image: url(<?php echo the_field('isotope_image', $singlePost->ID ); ?>);">
                    </div>
                    <div class="project__content">
                        <h3><?php echo $singlePost->post_title; ?></h3>
                    </div>
                </a>
            </div>
            <?php endforeach;
            wp_reset_postdata(); ?>
        </div>
    </div>
</section>
<section id="contact" class="section">
    <div class="container">
        <div class="contact__header section__header col-xs-12">
            <h1><?php the_field('contact_header'); ?></h1>
            <p><?php the_field('contact_subheader'); ?></p>
        </div>
        <div class="row">
            <div class="column__left column col-md-4 col-xs-12">
                <div class="contact__text contact__info">
                    <h2><?php the_field('column_header'); ?></h2>
                    <p><?php the_field('column_subheader'); ?></p>
                </div>
                <div class="contact__data contact__info">
                    <h2><?php the_field('contact_data'); ?></h2>
                    <a class="contact__phone contact__single" href="tel:662639050">
                        <i class="fa fa-phone" aria-hidden="true"></i><?php the_field('contact_phone'); ?>
                    </a>
                    <a class="contact__email contact__single" href="mailto:<?php the_field('contact_mail'); ?>">
                        <i class="fa fa-envelope" aria-hidden="true"></i><?php the_field('contact_mail'); ?>
                    </a>
                    <a class="contact__linkedin contact__single" href="<?php the_field('contact_linkedin'); ?>">
                        <i class="fa fa-linkedin" aria-hidden="true"></i>
                    </a>
                    <a class="contact__facebook contact__single" href="<?php the_field('contact_facebook'); ?>">
                        <i class="fa fa-facebook" aria-hidden="true"></i>
                    </a>
                </div>
            </div>
            <div class="column__right column col-md-8 col-xs-12">
                <div id="form">
                    <h2><?php the_field('form_header'); ?></h2>
                    <form>
                        <input type="text" placeholder="Imię i nazwisko*" class="input input__text">
                        <input type="email" placeholder="E-mail*" class="input input__text">
                        <textarea placeholder="Wiadomość*" class="input input__text"></textarea>
                        <input type="reset" value="Wyczyść" class="input input__button">
                        <input type="submit" value="Wyślij wiadomość" class="input input__button">
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>