function getClassyLoader($circle, text) {

    return $circle.ClassyLoader({
        speed: 20,
        diameter: 80,
        roundedLine: true,
        fontSize: '36px',
        fontFamily: 'Railway',
        fontColor: '#333333',
        lineColor: '#1fece5',
        percentage: 0,
        lineWidth: 15,
        start: 'top',
        remainingLineColor: '#0b4846',
        animate: false,
        showText: false,
        showHeader: true,
        header: text,
        fontWeight: 'normal'
    });
}

function progressBar(i, value) {

    $('#line' + i + ' div').animate({
        width: value
    }, 1500);
}

function setFooterYear() {
    var date = new Date();
    $('footer #year').text(date.getFullYear());
}

$(document).ready(function () {

    setFooterYear();

    $(window).scroll(function () {

        if ($(window).scrollTop() > 50) {

            $('#header').addClass('fixed');

        } else {

            $('#header').removeClass('fixed');

        }

        if($('#skills').length) {
            setLoaderPercent();
            setLinePercent();
        }

        if($('#numbers').length) {
            numberCounter();
        }

        if($('#header #menu').length) {
            activeMenu();
        }
    });
    /****************** SLICK MENU ************************************************************************************/

    $(document).on('click', '#mobile__button', function () {
        var mobileMenu = $('#mobile__menu');

        if (mobileMenu.is(':hidden')) {
            mobileMenu.show();
            $('#header').addClass('fixed');
        } else {
            mobileMenu.hide();

            if ($(window).scrollTop() < 50) {
                $('#header').removeClass('fixed');
            }
        }
    });

    $(window).resize(function () {
        var mobileMenu = $('#mobile__menu');

        if (!mobileMenu.is(':hidden')) {
            mobileMenu.hide();

            if ($('#header').hasClass('fixed')) {
                $('#header').removeClass('fixed');
            }
        }
    });
    /****************** SLICK *****************************************************************************************/

    if($('#hero__slider').length) {
        $('#hero__slider').slick({
            dots: true,
            infinite: true,
            autoplay: true,
            autoplaySpeed: 6000,
            arrows: false,
            slidesToShow: 1,
            fade: true,
            cssEase: 'linear'
        });
    }

    /****************** SLOW SCROLLING ********************************************************************************/

    $('a[href^="#"]').on('click', function (event) {

        var target = $($(this).attr('href'));

        if (target.length) {
            event.preventDefault();
            $('body').animate({
                scrollTop: (target.offset().top - 130)
            }, 1000);
        }

        if (!$('#mobile__menu').is(':hidden')) {
            $('#mobile__menu').hide();
        }
    });

    /******************** LOADER **************************************************************************************/

    var firstCircle = getClassyLoader($('#firstCircle'), 'HTML');
    var secondCircle = getClassyLoader($('#secondCircle'), 'CSS');
    var thirdCircle = getClassyLoader($('#thirdCircle'), 'Java');
    var fourthCircle = getClassyLoader($('#fourthCircle'), 'Spring');

    var checkCircle = 0;

    function setLoaderPercent() {
        var scrollTop = $(window).scrollTop();
        var positionCircle = $("#skills").position().top;
        var windowHeight = $('#home').height();
        var diffrence = scrollTop + (windowHeight / 3);

        if (diffrence > positionCircle && checkCircle == 0) {
            checkCircle = 1;

            firstCircle.setPercent(80).draw();
            secondCircle.setPercent(85).draw();
            thirdCircle.setPercent(70).draw();
            fourthCircle.setPercent(65).draw();
        }
    };

    /******************** LINE PROGRESS BAR ***************************************************************************/
    var values = new Array(130, 110, 140, 140, 170, 110, 160, 150, 120, 130, 70, 80);
    var linesPosition = $("#skills .lines").position().top;

    function setLinePercent() {
        var scrollTop = $(window).scrollTop();
        var windowHeight = $('#home').height()

        if (scrollTop + windowHeight / 3 > linesPosition) {
            for (var i = 1; i <= values.length; i++) {
                progressBar(i, values[i - 1]);
            }
        }
    }

    /******************** NUMBERS COUNTER *****************************************************************************/
    var checkCounter = 0;
    var firstNumber = 150;
    var secondNumber = 60;
    var thirdNumber = 50;
    var fourthNumber = 100;
    var i = 0;

    function numberCounter() {
        var numbersPosition = $("#numbers").position().top;

        if ($(window).scrollTop() + $('#home').height() / 2 > numbersPosition) {
            count();
        }
    }

    function count() {
        checkCounter = 1;

        if (i <= firstNumber)
            $('#numbers .firstNumber span').text(i);

        if (i <= secondNumber)
            $('#numbers .secondNumber span').text(i);

        if (i <= thirdNumber)
            $('#numbers .thirdNumber span').text(i);

        if (i <= fourthNumber)
            $('#numbers .fourthNumber span').text(i);

        i++;

        if (i <= firstNumber || i <= secondNumber || i <= thirdNumber || i <= fourthNumber)
            setTimeout(count, 30);
    }

    /********************** ISOTOPE ***********************************************************************************/

    var grid = $('.projects__wrapper').isotope({});

    $('.buttons__wrapper .button').on('click', function (event) {
        event.preventDefault();

        var filterValue = $(this).attr('data-filter');
        grid.isotope({filter: filterValue});
    });

    /********************** ACTIVE MENU *******************************************************************************/

    function activeMenu() {
        var homeStart = $('#home').offset().top, homeEnd = homeStart + $('#home').height();
        var aboutStart = $('#about').offset().top, aboutEnd = aboutStart + $('#about').height();
        var servicesStart = $('#services').offset().top, servicesEnd = servicesStart + $('#services').height();
        var skillsStart = $('#skills').offset().top, skillsEnd = skillsStart + $('#skills').height();
        var projectsStart = $('#projects').offset().top, projectsEnd = projectsStart + $('#projects').height();
        var contactStart = $('#contact').offset().top, contactEnd = contactStart + $('#contact').height();
        var tempScroll = $(window).scrollTop() + 131;

        if (tempScroll >= homeStart && tempScroll < homeEnd) {
            $('#menu a[href="#home"]').addClass('active');
        } else {
            $('#menu a[href="#home"]').removeClass('active');
        }

        if (tempScroll >= aboutStart && tempScroll < aboutEnd) {
                $('#menu a[href="#about"]').addClass('active');
        } else {
            $('#menu a[href="#about"]').removeClass('active');
        }

        if (tempScroll >= servicesStart && tempScroll < servicesEnd) {
            $('#menu a[href="#services"]').addClass('active');
        } else {
            $('#menu a[href="#services"]').removeClass('active');
        }

        if (tempScroll >= skillsStart && tempScroll < skillsEnd) {
            $('#menu a[href="#skills"]').addClass('active');
        } else {
            $('#menu a[href="#skills"]').removeClass('active');
        }

        if (tempScroll >= projectsStart && tempScroll < projectsEnd) {
            $('#menu a[href="#projects"]').addClass('active');
        } else {
            $('#menu a[href="#projects"]').removeClass('active');
        }

        if (tempScroll >= contactStart && tempScroll < contactEnd) {
            $('#menu a[href="#contact"]').addClass('active');
        } else {
            $('#menu a[href="#contact"]').removeClass('active');
        }
    }

    /********************** OTHER *************************************************************************************/


});